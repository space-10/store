<?php
namespace Divecheck\Store;

return [
    'driver'                   => [
        __NAMESPACE__ . '_driver' => [
            'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
            'cache' => 'array',
            'paths' => [
                __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'
            ]
        ],
        'orm_default'             => [
            'drivers' => [
                __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
            ]
        ]
    ],
];