<?php
namespace Divecheck\Store;

return [
    'doctrine'        => include __DIR__ . DS . 'doctrine.config.php',
    'controllers'     => [
        'invokables'   => [
            'Divecheck\Store\Admin\Controller\Store' => 'Divecheck\Store\Admin\Controller\StoreController'
        ],
        'initializers' => [
            // initializer to inject the store manager
            'Divecheck\Store\StoreManagerInitializer' => 'Divecheck\Store\StoreManager\Initializer\StoreManagerInitializer'
        ],
    ],
    'service_manager' => [
        'initializers' => [
            // initializer to inject the store manager
            'Divecheck\Store\StoreManagerInitializer' => 'Divecheck\Store\StoreManager\Initializer\StoreManagerInitializer'
        ],
        // 'Application' => 'Divecheck\Core\Mvc\Service\ApplicationFactory',
        // 'translator' => 'Divecheck\Core\I18n\Translator\TranslatorServiceFactory'
//        'aliases' => [
//            'StoreManager' => 'Divecheck\Core\StoreManager\StoreManager',
//            'Divecheck\Core\Db\Adapter' => 'Zend\Db\Adapter\Adapter'
//        ]
    ],
    'router'          => [
        'routes' => [
            'admin' => [
                'child_routes' => [
                    'system_store' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/system_store[/:action[/:id]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                                'id'     => '[0-9]+'
                            ],
                            'defaults'    => [
                                'controller' => 'Divecheck\Core\Admin\Controller\Store',
                                'action'     => 'index'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],
    'translator'      => [
        'locale'                    => 'en_US',
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo'
            ]
        ]
    ],
    'view_helpers'    => [
        'invokables' => [
            'store' => 'Divecheck\Core\View\Helper\Store',
        ],
    ],
    'view_manager'    => [
        // 'layout' => 'empty',
//        'template_map' => [
//            'error/404' => __DIR__ . '/../view/error/404.phtml',
//            'error/index' => __DIR__ . '/../view/error/index.phtml',
//            'layout/layout' => __DIR__ . '/../view/layout/default.phtml'
//        ],
        'template_path_stack' => [
            /* 'core' => */
            __NAMESPACE__ => __DIR__ . '/../view'
        ]
    ]
];
