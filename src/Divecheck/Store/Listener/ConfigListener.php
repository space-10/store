<?php
namespace Divecheck\Store\Listener;

use Divecheck\SystemSettings\Service\ConfigManager;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ConfigListener
 * @package Divecheck\Store\Listener
 */
class ConfigListener extends AbstractListenerAggregate implements ServiceLocatorAwareInterface, ObjectManagerAwareInterface
{

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected $objectManager;

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('Divecheck\SystemSettings\Service\ConfigManager',
            ConfigManager::EVENT_LOADED, [$this, 'loadConfig']);
    }

    public function loadConfig(MvcEvent $e)
    {
        $xmlConfig = $e->getParam('config');
        $storeRepo = $this->getObjectManager()->getRepository('Divecheck\Store\Entity\Store');
        $storeGroupRepo = $this->getObjectManager()->getRepository('Divecheck\Store\Entity\StoreGroup');
        $websiteRepo = $this->getObjectManager()->getRepository('Divecheck\Store\Entity\Website');

        $storeEntities = $storeRepo->findAll();
        $groupEntities = $storeGroupRepo->findAll();
        $websiteEntities = $websiteRepo->findAll();

        $websites = [];
        $stores = [];

        foreach ($websiteEntities as $w) {
            /* @var $w \Divecheck\Store\Entity\Website */
            $xmlConfig->setNode('websites/' . $w->getCode() . '/system/website/id', $w->getId());
            $xmlConfig->setNode('websites/' . $w->getCode() . '/system/website/name', $w->getName());
            $websites[$w->getId()] = [
                'code' => $w->getCode()
            ];
        }

        $substFrom = [];
        $substTo = [];
        foreach ($groupEntities as $g) {
            /* @var $s \Divecheck\Store\Entity\Store */
            $xmlConfig->setNode('stores/' . $s['code'] . '/system/store/id', $s->getId());
            $xmlConfig->setNode('stores/' . $s['code'] . '/system/store/name', $s->getName());
            $xmlConfig->setNode('stores/' . $s['code'] . '/system/website/id', $s->getWebsite()->getId());
            $xmlConfig->setNode('websites/' . $websites[$s->getWebsite()->getId()]['code'] . '/system/stores/' . $s->getCode(), $s->getId());
            $stores[$s->getId()] = [
                'code' => $s->getCode()
            ];
            $websites[$s->getWebsite()->getId()]['stores'][$s->getId()] = $s['code'];
        }


        foreach ($storeEntities as $s) {
            /* @var $row \Divecheck\Core\Entity\Config */
            if ($row->getScope() !== 'default')
            {
                continue;
            }

            $value = str_replace($substFrom, $substTo, $row->getValue());
            $xmlConfig->setNode('default/' . $row->getPath(), $value);
        }

    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return $this
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Set the object manager
     *
     * @param ObjectManager $objectManager
     * @return $this
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        return $this;
    }

    /**
     * Get the object manager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
