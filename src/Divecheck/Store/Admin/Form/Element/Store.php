<?php
namespace Divecheck\Core\Admin\Form\Element;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Divecheck\Core\StoreManager\StoreManagerAwareInterface;
use Divecheck\Core\StoreManager\StoreManagerInterface;

class Store extends Select implements ServiceLocatorAwareInterface, StoreManagerAwareInterface
{

    /**
     *
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     *
     * @var \Divecheck\Core\StoreManager\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Store selector
     *
     * If option 'with_defaults' is set to true the default stores will be included. Default is false.
     * You must not instantiate this class directly. Use
     * $this->getServiceLocator()->get('FormElementManager')->get('Divecheck\Core\Admin\Form\Element', $options) instead
     *
     *
     * @param string $name
     * @param array $options
     */
    public function __construct($name = null, array $options = [])
    {

        parent::__construct('admin_store_select', $options);
    }

    /**
     * Returns true whether defaults should be loaded
     *
     * @return boolean
     */
    protected function isLoadWithDefaults()
    {

        return (bool) $this->getOption('with_defaults');
    }

    /*
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {

        return $this->serviceLocator;
    }

    /*
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {

        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerAwareInterface::setStoreManager()
     */
    public function setStoreManager(StoreManagerInterface $storeManager)
    {

        $this->storeManager = $storeManager;
    }

    /*
     * (non-PHPdoc)
     * @see \Zend\Form\Element::init()
     */
    public function init()
    {

        $valueOptions = [];
        $websites = $this->storeManager->getWebsites($this->isLoadWithDefaults());
        foreach ($websites as $website)
        {
            $websiteOption = [
                'label' => $website->getName(),
                'value' => $website->getCode(),
                'attributes' => [
                    'data-website' => $website->getId()
                ]
            ];
            $groups = $this->storeManager->getStoreGroups($website, $this->isLoadWithDefaults());
            foreach ($groups as $group)
            {
                $storeGroupOption = [
                    'label' => $group->getName()
                ];

                $stores = $this->storeManager->getStoresByGroup($group);
                foreach ($stores as $store)
                {
                    $storeOption = [
                        'label' => $store->getName(),
                        'value' => $store->getCode(),
                        'attributes' => [
                            'data-website' => $store->getId()
                        ]
                    ];
                    $storeGroupOption['options'][] = $storeOption;
                }
                $websiteOption['options'][] = $storeGroupOption;
            }
            $valueOptions[] = $websiteOption;
        }

        $this->setValueOptions($valueOptions);
    }
}
