<?php
namespace Divecheck\Core\Admin\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class StoreSwitch extends Form implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    public function init()
    {

        $storeSelect = $this->getServiceLocator()
            ->get('FormElementManager')
            ->get('Divecheck\Core\Admin\Form\Element\SelectStore', [
            'with_defaults' => true
        ]);
        $this->add($storeSelect);
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {

        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {

        return $this->serviceLocator;
    }
}
