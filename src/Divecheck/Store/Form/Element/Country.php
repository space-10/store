<?php
namespace Divecheck\Core\Form\Element;

use Zend\Form\Element\Select;

/**
 *
 * Short description for Core\Form\Element$Country
 *
 * Long description for Core\Form\Element$Country
 *
 * @copyright Copyright (c) 2013 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @license http://www.sirrus-systems.de/products/divecheck/license Divecheck Proprietary License
 * @version $Id$
 * @since Class available since revision $Revision$
 */
class Country extends Select
{

    /**
     * Constructor
     *
     * @param string $name
     * @param array $options
     */
    public function __construct($name = null, $options = array())
    {

        parent::__construct($name ? $name : 'country', $options);

        $this->setValueOptions(array(
            'GER' => __('Germany'),
            'USA' => __('United States of America')
        ));
    }
}
