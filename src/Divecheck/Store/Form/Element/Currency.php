<?php
namespace Divecheck\Core\Form\Element;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Form\FormElementManager;

class Currency extends Select implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    /**
     * Constructor
     *
     * @param string $name
     * @param array $options
     */
    public function __construct($name = null, array $options = array())
    {

        parent::__construct($name ? $name : 'currency', $options);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {

        if ($serviceLocator instanceof FormElementManager)
        {
            $serviceLocator = $serviceLocator->getServiceLocator();
        }
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {

        return $this->serviceLocator;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\Form\Element::init()
     */
    public function init()
    {

        $adminConfig = $this->serviceLocator->get('Divecheck\Config');
        /* @var $services \Sirrus\Simplexml\Element */
        // FIXME: move xml path to constant
        // FIXME: move to some stuff which has the currency code AND name instead of only providing the code
        $services = $adminConfig->getNode('default/system/currency/installed');
        $services = explode(",", (string) $services);
        foreach ($services as $code)
        {
            $options[$code] = $code;
        }

        $this->setValueOptions($options);
    }
}
