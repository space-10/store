<?php
namespace Divecheck\Core\Form;

use Sirrus\Form\ORMForm;
use Zend\Form\Element\Csrf;
use Divecheck\Core\Entity\Store as StoreEntity;

class StoreGroup extends ORMForm
{

    public function __construct($name = null, array $options = [])
    {

        parent::__construct($name ? $name : 'core_store_group_form', $options);
        $this->setAttribute('id', $this->getName());
        $this->setObject(new StoreEntity());
        // $this->setHydrator(new \Zend\Stdlib\Hydrator\Reflection());
    }

    public function init()
    {
        // http://www.acnenomor.com/809650p1/zf2how-to-set-dependent-dropdown-using-ajax-on-zend-form
        $this->add(new Csrf('token'));

        $this->add(
            [
                'name' => 'name',
                'options' => [
                    'label' => __('Store Name')
                ],
                'attributes' => [
                    'required' => 'required'
                ]
            ]);
        $this->add(
            [
                'type' => 'objectselect',
                'name' => 'website',
                'options' => [
                    'object_manager' => $this->getObjectManager(),
                    'target_class' => 'Divecheck\Core\Entity\Website',
                    'property' => 'name',
                    'is_method' => true,
                    'label' => __('Website'),
                    // 'empty_option' => '--- please choose ---',
                    'find_method' => [
                        'name' => 'findAll'
                    ]
                ],
                'attributes' => [
                    'required' => 'required'
                ]
            ]);

        $this->add(
            [
                'type' => 'objectselect',
                'name' => 'defaultStore',
                'options' => [
                    'object_manager' => $this->getObjectManager(),
                    'target_class' => 'Divecheck\Core\Entity\Store',
                    'property' => 'name',
                    'is_method' => true,
                    'label' => __('Default Store'),
                    'find_method' => [
                        'name' => 'findAll'
                    ]
                ],
                'attributes' => [
                    'required' => 'required'
                ]
            ]);
        $this->add(
            [
                'type' => 'objectselect',
                'name' => 'rootCategory',
                'options' => [
                    'object_manager' => $this->getObjectManager(),
                    'target_class' => 'Divecheck\Catalog\Entity\Category',
                    'property' => 'path',
                    'is_method' => true,
                    'label' => __('Default Root Category'),
                    'find_method' => [
                        'name' => 'findAll'
                    ]
                ],
                'attributes' => [
                    'required' => 'required'
                ]
            ]);
    }
}
