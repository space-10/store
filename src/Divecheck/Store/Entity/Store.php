<?php
namespace Divecheck\Store\Entity;

use Divecheck\Core\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Store
 * @ORM\Entity
 * @ORM\Table(name="core_store")
 * @package Divecheck\Store\Entity
 */
class Store extends AbstractEntity
{

    const DEFAULT_STORE_ID = 0;

    const DEFAULT_STORE_CODE = 'default';

    /**
     * @ORM\Id
     * @ORM\Column(name="store_id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false, length=32)
     *
     * @var string
     */
    protected $code;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Divecheck\Store\Entity\Website", inversedBy="stores", fetch="EAGER")
     * @ORM\JoinColumn(name="website_id", referencedColumnName="website_id")
     *
     * @var Website
     */
    protected $website;

    /**
     * @ORM\ManyToOne(targetEntity="\Divecheck\Store\Entity\StoreGroup", inversedBy="stores", fetch="EAGER")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="group_id")
     *
     * @var StoreGroup
     */
    protected $group;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=false, length=10, options={"unsigned":true, "default":0, "comment":"Sort order"})
     *
     * @var integer
     */
    protected $sortOrder;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=false, length=1, options={"unsigned":true, "default":1, "comment":"Active state of a store"})
     *
     * @var boolean
     */
    protected $isActive;

    /**
     * {@inheritDoc}
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId($id)
    {

        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     *
     * @param string $code
     * @return \Divecheck\Store\Entity\Store
     */
    public function setCode($code)
    {

        $this->code = $code;
        return $this;
    }

    /**
     *
     * @return \Divecheck\Store\Entity\Website
     */
    public function getWebsite()
    {

        return $this->website;
    }

    /**
     *
     * @param Website $website
     * @return \Divecheck\Store\Entity\Store
     */
    public function setWebsite(Website $website)
    {

        if ($this->website->equals($website)) {
            return $this;
        }

        $this->website->removeStore($this, $this->group);
        $this->website = $website;
        $this->website->addStore($this, $this->group);

        return $this;
    }

    /**
     *
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function getGroup()
    {

        return $this->group;
    }

    /**
     *
     * @param StoreGroup $group
     * @return \Divecheck\Store\Entity\Store
     */
    public function setGroup(StoreGroup $group)
    {

        if ($this->group->equals($group)) {
            return $this;
        }

        $this->group->removeStore($this);
        $this->group = $group;
        $this->group->addStore($this);

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     *
     * @param integer $name
     * @return \Divecheck\Store\Entity\Store
     */
    public function setName($name)
    {

        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSortOrder()
    {

        return $this->sortOrder;
    }

    /**
     *
     * @param integer $sortOrder
     * @return \Divecheck\Store\Entity\Store
     */
    public function setSortOrder($sortOrder)
    {

        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getIsActive()
    {

        return $this->isActive();
    }

    /**
     *
     * @param boolean $isActive
     * @return \Divecheck\Store\Entity\Store
     */
    public function setIsActive($isActive)
    {

        return $this->setActive($isActive);
    }

    /**
     *
     * @return boolean
     */
    public function isActive()
    {

        return $this->isActive;
    }

    /**
     *
     * @param boolean $active
     * @return \Divecheck\Store\Entity\Store
     */
    public function setActive($active)
    {

        $this->isActive = (bool)$active;
        return $this;
    }
}
