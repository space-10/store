<?php
namespace Divecheck\Store\Entity;

use Divecheck\Core\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Website
 * @ORM\Entity
 * @ORM\Table(name="core_store_website")
 * @package Divecheck\Store\Entity
 */
class Website extends AbstractEntity
{

    const DEFAULT_WEBSITE_ID = 0;

    const DEFAULT_WEBSITE_CODE = 'default';

    /**
     * @ORM\Id
     * @ORM\Column(name="website_id", type="integer", nullable=false);
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", type="string", nullable=false)
     *
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     *
     * @var integer
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="\Divecheck\Store\Entity\StoreGroup", fetch="EAGER")
     * @ORM\JoinColumn(name="default_group_id", referencedColumnName="group_id")
     *
     * @var StoreGroup
     */
    protected $defaultGroup;

    /**
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     *
     * @var boolean
     */
    protected $isDefault;

    /* ADDITIONAL MAPPINGS */

    /**
     * @ORM\OneToMany(targetEntity="Divecheck\Store\Entity\StoreGroup", mappedBy="website", fetch="EAGER")
     *
     * @var ArrayCollection
     */
    protected $storeGroups;

    /**
     * @ORM\OneToMany(targetEntity="Divecheck\Store\Entity\Store", mappedBy="website", fetch="EAGER")
     *
     * @var ArrayCollection
     */
    protected $stores;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->storeGroups = new ArrayCollection();
        $this->stores = new ArrayCollection();
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sirrus\Entity\AbstractORMEntity::getId()
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sirrus\Entity\AbstractORMEntity::setId()
     */
    public function setId($id)
    {

        $this->id = (int) $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     *
     * @param string $code
     * @return \Divecheck\Core\Entity\Website
     */
    public function setCode($code)
    {

        $this->code = $code;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return \Divecheck\Core\Entity\Website
     */
    public function setName($name)
    {

        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSortOrder()
    {

        return $this->sortOrder;
    }

    /**
     *
     * @param integer $sortOrder
     * @return \Divecheck\Core\Entity\Website
     */
    public function setSortOrder($sortOrder)
    {

        $this->sortOrder = (int) $sortOrder;
        return $this;
    }

    /**
     *
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function getDefaultGroup()
    {

        return $this->defaultGroup;
    }

    /**
     *
     * @param StoreGroup $defaultGroup
     * @return \Divecheck\Core\Entity\Website
     */
    public function setDefaultGroup(StoreGroup $defaultGroup)
    {

        $this->defaultGroup = $defaultGroup;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getIsDefault()
    {

        return $this->isDefault();
    }

    /**
     *
     * @param boolean $isDefault
     * @return \Divecheck\Core\Entity\Website
     */
    public function setIsDefault($isDefault)
    {

        return $this->setDefault($isDefault);
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getStoreGroups()
    {

        return $this->storeGroups;
    }

    /**
     *
     * @param ArrayCollection $storeGroups
     * @return \Divecheck\Core\Entity\Website
     */
    public function setStoreGroups(ArrayCollection $storeGroups)
    {

        $this->storeGroups = $storeGroups;
        return $this;
    }

    /**
     * Adds a new store group to this website
     *
     * @param StoreGroup $storeGroup
     * @return \Divecheck\Core\Entity\Website
     */
    public function addStoreGroup(StoreGroup $storeGroup)
    {

        if (! $this->storeGroups->contains($storeGroup))
        {
            $this->storeGroups->add($storeGroup);
        }
        return $this;
    }

    /**
     * Removes a StoreGroup from the Website
     *
     * @param StoreGroup $storeGroup
     * @return boolean True on success, false if removal of the store-group failed
     */
    public function removeStoreGroup(StoreGroup $storeGroup)
    {

        if ($this->defaultGroup->equals($storeGroup))
        {
            throw new Exception\StoreGroupException('The default store group cannot be removed.');
        }

        return $this->storeGroups->removeElement($storeGroup);
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getStores()
    {

        return $this->stores;
    }

    /**
     *
     * @param ArrayCollection $stores
     * @return \Divecheck\Core\Entity\Website
     */
    public function setStores(ArrayCollection $stores)
    {

        $this->stores = $stores;
        return $this;
    }

    /**
     * Add a store to the current website.
     *
     * The store will be added to $storeGroup if it is not null, otherwise it will be added to the default group. If the group is
     * not yet added to this website
     * the $storeGroup will be added to the groups of this website.
     *
     * @param Store $store
     * @param StoreGroup $storeGroup
     * @return \Divecheck\Core\Entity\Website
     */
    public function addStore(Store $store, StoreGroup $storeGroup = null)
    {

        if ($storeGroup === null)
        {
            $storeGroup = $this->getDefaultGroup();
        }

        $storeGroup->addStore($store);

        if (! $this->storeGroups->contains($storeGroup))
        {
            $this->addStoreGroup($storeGroup);
        }

        return $this;
    }

    /**
     * Removes a store from this website.
     *
     * Removes $store from $storeGroup if it is not null, otherwise $store will be removed from the default group. If it was not
     * found in the default group the $store will be removed from all groups.
     *
     * @param Store $store
     * @param StoreGroup $storeGroup
     */
    public function removeStore(Store $store, StoreGroup $storeGroup = null)
    {

        if ($storeGroup === null)
        {
            $storeGroup = $this->getDefaultGroup();
        }

        if (! $storeGroup->hasStore($store) && ! $this->storeGroups->isEmpty())
        {
            foreach ($this->storeGroups as $storeGroup)
            {
                $storeGroup->removeStore($store);
            }
            return true;
        }

        return $storeGroup->removeStore($store);
    }

    /**
     *
     * @return boolean
     */
    public function isDefault()
    {

        return (bool) $this->isDefault;
    }

    /**
     *
     * @param boolean $isDefault
     * @return \Divecheck\Core\Entity\Website
     */
    public function setDefault($isDefault)
    {

        $this->isDefault = (bool) $isDefault;
        return $this;
    }
}
