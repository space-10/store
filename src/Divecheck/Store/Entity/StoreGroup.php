<?php
namespace Divecheck\Store\Entity;

use Divecheck\Core\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Divecheck\Catalog\Entity\Category;

/**
 * Class StoreGroup
 * @ORM\Entity
 * @ORM\Table(name="core_store_group")
 * @package Divecheck\Store\Entity
 */
class StoreGroup extends AbstractEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Divecheck\Store\Entity\Website", inversedBy="storeGroups", fetch="EAGER")
     * @ORM\JoinColumn(name="website_id", referencedColumnName="website_id")
     *
     * @var Website
     */
    protected $website;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $name;

    /**
     * @ ORM \ ManyToOne(targetEntity="Divecheck\Catalog\Entity\Category", fetch="EAGER")
     * @ ORM \ JoinColumn(name="root_category_id", referencedColumnName="entity_id")
     *
     * @todo fix cycle dependency
     * @var Category
     */
    protected $rootCategory;

    /**
     * @ORM\ManyToOne(targetEntity="\Divecheck\Store\Entity\Store", fetch="EAGER")
     * @ORM\JoinColumn(name="default_store_id", referencedColumnName="store_id")
     *
     * @var Store
     */
    protected $defaultStore;

    /* ADDITIONAL MAPPINGS */

    /**
     * @ORM\OneToMany(targetEntity="Divecheck\Store\Entity\Store", mappedBy="group")
     *
     * @var ArrayCollection
     */
    protected $stores;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->stores = new ArrayCollection();
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sirrus\Entity\AbstractORMEntity::getId()
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Sirrus\Entity\AbstractORMEntity::setId()
     */
    public function setId($id)
    {

        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return \Divecheck\Core\Entity\Website
     */
    public function getWebsite()
    {

        return $this->website;
    }

    /**
     *
     * @param Website $website
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function setWebsite(Website $website)
    {

        $this->website = $website;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function setName($name)
    {

        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return \Divecheck\Catalog\Entity\Category
     */
    public function getRootCategory()
    {

        return $this->rootCategory;
    }

    /**
     *
     * @param Category $rootCategory
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function setRootCategory(Category $rootCategory)
    {

        $this->rootCategory = $rootCategory;
        return $this;
    }

    /**
     *
     * @return \Divecheck\Core\Entity\Store
     */
    public function getDefaultStore()
    {

        return $this->defaultStore;
    }

    /**
     *
     * @param StoreGroup $defaultStore
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function setDefaultStore(StoreGroup $defaultStore)
    {

        $this->defaultStore = $defaultStore;
        return $this;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getStores()
    {

        return $this->stores;
    }

    /**
     *
     * @param ArrayCollection $stores
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function setStores(ArrayCollection $stores)
    {

        $this->stores = $stores;
        return $this;
    }

    /**
     * Checks if a store is assigned to this group.
     *
     * @param Store $store
     * @return boolean
     */
    public function hasStore(Store $store)
    {

        return $this->stores->contains($store);
    }

    /**
     *
     * @param Store $store
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function addStore(Store $store)
    {

        if (! $this->stores->contains($store))
        {
            $this->stores->add($store);
        }
        return $this;
    }

    /**
     *
     * @param Store $store
     * @return boolean
     */
    public function removeStore(Store $store)
    {

        if ($this->defaultStore->equals($store))
        {
            throw new Exception\StoreException('The default store cannot be removed.');
        }

        return $this->stores->removeElement($store);
    }
}
