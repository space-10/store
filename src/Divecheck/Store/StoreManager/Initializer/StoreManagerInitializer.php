<?php
namespace Divecheck\Store\StoreManager\Initializer;

use Zend\ServiceManager\InitializerInterface;
use Divecheck\Core\StoreManager\StoreManagerAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class StoreManagerInitializer implements InitializerInterface
{
    /*
     * (non-PHPdoc) @see \Zend\ServiceManager\InitializerInterface::initialize()
     */
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {

        if ($instance instanceof StoreManagerAwareInterface)
        {
            $instance->setStoreManager($serviceLocator->get('StoreManager'));
        }
    }
}
