<?php
namespace Divecheck\Core\StoreManager;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class StoreManagerFactory implements FactoryInterface
{
    /*
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->get('Divecheck\Core\StoreManager\StoreManagerInvokable');
        $sm->init();
        return $sm;
    }
}
