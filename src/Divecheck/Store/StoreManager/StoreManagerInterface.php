<?php
namespace Divecheck\Core\StoreManager;

interface StoreManagerInterface
{

    /**
     * Returns website by code or id
     *
     * If param $id is null, the current website is returned (based on default settings).
     *
     * @param string|int $id
     * @return \Divecheck\Core\Entity\Website
     */
    public function getWebsite($id = null);

    /**
     * Returns all loaded websites
     *
     * If $withDefault is set to true the default websites (id = 0) will also be returned.
     *
     * @param string $withDefault
     * @return \Divecheck\Core\Entity\Website[]
     */
    public function getWebsites($withDefault = false);

    public function getDefaultWebsite();

    /**
     * Returns store group by id
     *
     * @return \Divecheck\Core\Entity\StoreGroup
     */
    public function getStoreGroup($id);

    /**
     * Returns loaded store groups
     *
     * If parameter $website is not null the groups will be filtered by website
     * If $withDefault is set to true the default store groups (id = 0) will also be returned.
     *
     * @param null|string|int|\Divecheck\Core\Entity\Website $website
     * @param string $withDefault
     * @return \Divecheck\Core\Entity\StoreGroup[]
     */
    public function getStoreGroups($website = null, $withDefault = false);

    /**
     * Returns store by code or id
     *
     * If param $id is null, the current store is returned (based on default settings).
     *
     * @param string|int $id
     * @return \Divecheck\Core\Entity\Store
     */
    public function getStore($id = null);

    /**
     * Returns all loaded stores
     *
     * If $withDefault is set to true the default stores (id = 0) will also be returned.
     *
     * @param string $withDefault
     * @return \Divecheck\Core\Entity\Store[]
     */
    public function getStores($withDefault = false);

    /**
     *
     * @param string|int|\Divecheck\Core\Entity\Website $website
     * @return \Divecheck\Core\Entity\Store[]
     */
    public function getStoresByWebsite($website);

    /**
     *
     * @param string|int|\Divecheck\Core\Entity\Website $website
     * @return \Divecheck\Core\Entity\Store
     */
    public function getDefaultStoreByWebsite($website);

    /**
     *
     * @param string|int|\Divecheck\Core\Entity\StoreGroup $storeGroup
     * @return \Divecheck\Core\Entity\Store
     */
    public function getDefaultStoreByGroup($storeGroup);

    /**
     *
     * @return \Divecheck\Core\Entity\Store
     */
    public function getDefaultStore();
}
