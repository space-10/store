<?php
namespace Divecheck\Core\StoreManager;

use Divecheck\Core\Entity\Website;
use Divecheck\Core\Entity\StoreGroup;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Stdlib\InitializableInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\EntityManager;

class StoreManager implements StoreManagerInterface, ServiceLocatorAwareInterface
{

    const DEFAULT_WEBSITE_ID = 0;

    const DEFAULT_STORE_ID = 0;

    /**
     *
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     *
     * @var \Divecheck\Core\Entity\Website[]
     */
    protected $websites;

    /**
     *
     * @var \Divecheck\Core\Entity\Website[]
     */
    protected $websitesByCodeOrId;

    /**
     *
     * @var \Divecheck\Core\Entity\StoreGroup[]
     */
    protected $groups;

    /**
     *
     * @var \Divecheck\Core\Entity\StoreGroup[]
     */
    protected $groupsById;

    /**
     *
     * @var \Divecheck\Core\Entity\Store[]
     */
    protected $stores;

    /**
     *
     * @var \Divecheck\Core\Entity\Store[]
     */
    protected $storesByCodeOrId;

    /**
     *
     * @var \Divecheck\Core\Entity\Store[]
     */
    protected $storesByWebsiteCodeOrId;

    /**
     *
     * @var \Divecheck\Core\Entity\Website
     */
    protected $defaultWebsite;

    /**
     *
     * @var \Divecheck\Core\Entity\Store
     */
    protected $defaultStore;

    /**
     *
     * @var \Divecheck\Core\Entity\Website
     */
    protected $currentWebsite;

    /**
     *
     * @var \Divecheck\Core\Entity\Store
     */
    protected $currentStore;

    protected $initialized = false;

    /**
     *
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $em)
    {

        $this->websites = [];
        $this->websitesByCodeOrId = [];
        $this->groups = [];
        $this->groupsById = [];
        $this->stores = [];
        $this->storesByCodeOrId = [];

        $this->entityManager = $em;
    }

    /*
     * (non-PHPdoc) @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {

        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /*
     * (non-PHPdoc) @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {

        return $this->serviceLocator;
    }

    /*
     * (non-PHPdoc)
     * @see \Zend\Stdlib\InitializableInterface::init()
     */
    public function init()
    {

        if ($this->initialized)
        {
            return $this;
        }

        $this->loadStores();

        // called automaticially after all dependencies are set

        // check config, load stores and websites

        $appConfig = $this->getServiceLocator()->get('ApplicationConfig');
        $cfg = $appConfig['store_manager']['init'];

        $scopeType = key($cfg);
        $scopeCode = $cfg[$scopeType];

        if (! $scopeCode && ! is_null($this->website))
        {
            $scopeCode = $this->website->getCode();
            $scopeType = 'website';
        }

        switch ($scopeType)
        {
            case 'store':
                $this->currentStore = $this->getStore($scopeCode);
                break;
            case 'group':
                $this->currentStore = $this->getDefaultStoreByGroup($scopeCode);
                break;
            case 'website':
                $this->currentStore = $this->getDefaultStoreByWebsite($scopeCode);
                break;
            default:
                throw new Exception\UnknownStoreTypeException('Unknown store type:' . $scopeType);
        }
        $this->initialized = true;
        // @TODO: load store from session, if any
        // if (!empty($this->currentStore)) {
        // $this->_checkCookieStore($scopeType);
        // $this->_checkGetStore($scopeType);
        // }
        // $this->_useSessionInUrl = $this->getStore()->getConfig(
        // Mage_Core_Model_Session_Abstract::XML_PATH_USE_FRONTEND_SID);
        return $this;
    }

    protected function loadStores()
    {

        $websiteRepository = $this->entityManager->getRepository('Divecheck\Core\Entity\Website');
        $groupRepository = $this->entityManager->getRepository('Divecheck\Core\Entity\StoreGroup');
        $storeRepository = $this->entityManager->getRepository('Divecheck\Core\Entity\Store');

        $websites = $websiteRepository->findAll();
        foreach ($websites as $website)
        {
            /* @var $website \Divecheck\Core\Entity\Website */
            $this->websites[] = $website;
            $this->websitesByCodeOrId[$website->getId()] = $website;
            $this->websitesByCodeOrId[$website->getCode()] = $website;
            if ($website->isDefault())
            {
                $this->defaultWebsite = $website;
                $this->defaultStore = $website->getDefaultGroup()->getDefaultStore();
            }
        }

        $stores = $storeRepository->findAll();
        foreach ($stores as $store)
        {
            /* @var $store \Divecheck\Core\Entity\Store */
            $this->stores[] = $store;
            $this->storesByCodeOrId[$store->getId()] = $store;
            $this->storesByCodeOrId[$store->getCode()] = $store;
            $this->storesByWebsiteCodeOrId[$store->getWebsite()->getId()][$store->getId()] = $store;
            $this->storesByWebsiteCodeOrId[$store->getWebsite()->getCode()][$store->getId()] = $store;
        }

        $groups = $storeRepository->findAll();
        foreach ($groups as $group)
        {
            /* @var $group \Divecheck\Core\Entity\StoreGroup */
            $this->groups[] = $group;
            $this->groupsById[$group->getId()] = $group;
        }
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getWebsite()
     */
    public function getWebsite($id = null)
    {

        if ($id === null || (! is_int($id) && ! is_string($id) && ! isset($this->websitesByCodeOrId[$id])))
        {
            return $this->getDefaultWebsite();
        }

        return $this->websitesByCodeOrId[$id];
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getWebsites()
     */
    public function getWebsites($withDefault = false)
    {

        if ($withDefault === true)
        {
            return $this->websites;
        }

        return $this->websites;
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getStore()
     */
    public function getStore($id = null)
    {

        if (! is_int($id) && ! is_string($id))
        {
            return $this->getDefaultStore();
        }

        if (! isset($this->storesByCodeOrId[$id]))
        {
            throw new Exception\InvalidStoreException('Store with id "' . $id . '" not found.');
        }

        return $this->storesByCodeOrId[$id];
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getStores()
     */
    public function getStores($withDefault = false)
    {

        $this->init();
        if ($withDefault === true)
        {
            return $this->stores;
        }
        return $this->stores;
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getStoresByWebsite()
     */
    public function getStoresByWebsite($website)
    {

        $this->init();
        if (! is_string($website) && ! is_int($website) && ! $website instanceof Website)
        {
            throw new Exception\InvalidArgumentException('$website must be type of \Divecheck\Core\Entity\Website, string or int; given: ' . gettype($website));
        }

        // $website is either string or int
        if (! $website instanceof Website)
        {
            $website = $this->websitesByCodeOrId[$website]->getId();
        }

        return isset($this->storesByWebsiteCodeOrId[$website]) ? $this->storesByWebsiteCodeOrId[$website] : [];
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getStoreGroup()
     */
    public function getStoreGroup($group)
    {

        $this->init();
        if (! $group instanceof StoreGroup && ! is_int($group))
        {
            throw new Exception\InvalidArgumentException('$group must be type of \Divecheck\Core\Entity\StoreGroup or int; given: ' . gettype($group));
        }

        // $website is either string or int
        if (! $group instanceof Website)
        {
            $group = $this->groupsById[$group];
        }

        return $group;
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getStoreGroups()
     */
    public function getStoreGroups($website = null, $withDefault = false)
    {

        $this->init();
        if (! $website instanceof Website && ! is_int($website) && ! is_string($website))
        {
            throw new Exception\InvalidArgumentException('$website must be type of \Divecheck\Core\Entity\Website, string or int; given: ' . gettype($website));
        }

        // $website is either string or int
        if (! $website instanceof Website)
        {
            $website = $this->websitesByCodeOrId[$website];
        }

        $groups = [];
        foreach ($this->groups as $group)
        {
            if ($group->getWebsite()->equals($website))
            {
                $groups[] = $group;
            }
        }
        return $groups;
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getDefaultStoreByWebsite()
     */
    public function getDefaultStoreByWebsite($website)
    {

        if (! $website instanceof Website && ! is_int($website) && ! is_string($website))
        {
            throw new Exception\InvalidArgumentException('$website must be type of \Divecheck\Core\Entity\Website, string or int; given: ' . gettype($website));
        }

        // $website is either string or int
        if (! $website instanceof Website)
        {
            $website = $this->websitesByCodeOrId[$website];
        }

        return $website->getDefaultGroup()->getDefaultStore();
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getDefaultStoreByGroup()
     */
    public function getDefaultStoreByGroup($group)
    {

        if (! $group instanceof StoreGroup && ! is_int($group) && ! is_string($group))
        {
            throw new Exception\InvalidArgumentException('$group must be type of \Divecheck\Core\Entity\StoreGroup, string or int; given: ' . gettype($group));
        }

        // $group is either string or int
        if (! $group instanceof StoreGroup)
        {
            $group = $this->groups[$group];
        }

        return $group->getDefaultStore();
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getDefaultStore()
     */
    public function getDefaultStore()
    {

        if (! $this->defaultStore)
        {
            $this->defaultStore = $this->getDefaultStoreByWebsite($this->getDefaultWebsite());
        }

        return $this->defaultStore;
    }

    /*
     * (non-PHPdoc)
     * @see \Divecheck\Core\StoreManager\StoreManagerInterface::getDefaultWebsite()
     */
    public function getDefaultWebsite()
    {

        // should be set in initialization. if not set yet we need to use the default website
        if (! $this->defaultWebsite)
        {
            if (! isset($this->websitesByCodeOrId[static::DEFAULT_WEBSITE_ID]))
            {
                foreach ($this->websites as $website)
                {
                    break;
                }
            }
            else
            {
                $website = $this->websitesByCodeOrId[static::DEFAULT_WEBSITE_ID];
            }

            $this->defaultWebsite = $website;
        }
        return $this->defaultWebsite;
    }
}
