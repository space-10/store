<?php
namespace Divecheck\Core\StoreManager;

interface StoreManagerAwareInterface
{

    public function setStoreManager(StoreManagerInterface $storeManager);
}
