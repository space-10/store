<?php
namespace Divecheck\Core\StoreManager\Exception;

class UnknownStoreTypeException extends \DomainException implements ExceptionInterface
{
}
