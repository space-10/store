<?php
namespace Divecheck\Core\StoreManager\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
