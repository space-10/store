<?php
namespace Divecheck\Store;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;

/**
 * Short description for Divecheck\Core$Module
 *
 * Long description for Divecheck\Core$Module
 *
 * @author Tobias Trozowski
 * @copyright Copyright (c) 2013 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @version $Id$
 * @since $Revision$
 * @link http://framework.zend.com/manual/2.0/en/modules/zend.console.introduction.html
 */
class Module implements ConfigProviderInterface, DependencyIndicatorInterface
{

    /*
     * (non-PHPdoc) @see \Zend\ModuleManager\Feature\DependencyIndicatorInterface::getModuleDependencies()
     */
    public function getModuleDependencies()
    {

        return [
            'DoctrineModule',
            'DoctrineORMModule'
        ];
    }

    /*
     * (non-PHPdoc) @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {

        return include __DIR__ . '/../../../etc/module.config.php';
    }

}