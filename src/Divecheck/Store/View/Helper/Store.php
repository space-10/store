<?php
namespace Divecheck\Core\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractPluginManager;
use Divecheck\Core\Mvc\Application;

/**
 *
 * Short description for Divecheck\Core\View\Helper$Store
 *
 * Long description for Divecheck\Core\View\Helper$Store
 *
 * @copyright Copyright (c) 2013 Sirrus Systems GmbH (http://www.sirrus-systems.de/)
 * @license http://www.sirrus-systems.de/products/divecheck/license Divecheck Proprietary License
 * @version $Id$
 * @since Class available since revision $Revision$
 */
class Store extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     *
     * @var \Zend\ServiceManager\AbstractPluginManager
     */
    protected $serviceLocator;

    /**
     *
     * @var \Divecheck\Core\Entity\Store
     */
    protected $store;

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {

        if ($serviceLocator instanceof AbstractPluginManager)
        {
            $serviceLocator = $serviceLocator->getServiceLocator();
        }
        $this->serviceLocator = $serviceLocator;

        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {

        return $this->serviceLocator;
    }

    /**
     *
     * @return \Divecheck\Core\Mvc\Application
     */
    public function getApplication()
    {

        return Application::app();
    }

    /**
     *
     * @param string $path
     * @return \Divecheck\Core\Entity\Store
     */
    public function __invoke()
    {

        if (! $this->store)
        {
            $this->store = $this->getApplication()->getStore();
        }

        return $this->store;
        ;
    }
}
