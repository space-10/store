<?php
namespace Divecheck\Core\EntityRepository;

use Divecheck\Core\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Class StoreRepository
 * @package Divecheck\Core\EntityRepository
 * Native queries: http://www.wjgilmore.com/blog/2014/04/09/the-power-of-doctrine-2-s-custom-repositories-and-native-queries/
 */
class StoreRepository extends EntityRepository
{


    /**
     * @param Entity\Website $website
     * @param array          $criteria
     * @param array          $orderBy
     * @param null           $limit
     * @param null           $offset
     *
     * @return array The objects.
     */
    public function findByWebsite(Entity\Website $website, array $criteria = [], array $orderBy = null, $limit = null, $offset = null){
        $criteria['website_id'] = $website->getId();
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param Entity\StoreGroup $group
     * @param array             $criteria
     * @param array             $orderBy
     * @param null              $limit
     * @param null              $offset
     *
     * @return array The objects.
     */
    public function findByGroup(Entity\StoreGroup $group, array $criteria = [], array $orderBy = null, $limit = null, $offset = null){
        $criteria['group_id'] = $group->getId();
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }
}
